#include <stdio.h>

int iterative(int n)
{
    if(n==0)
        return 0;
    if(n==1)
        return 1;

    int a=0;int b=1;int temp;

    for(int i=2;i<=n;i++)
    {
        temp=a+b;
        a=b;
        b=temp;
    }
    return b;
}

int recursive(int n)
{
    if(n==0)
        return 0;
    if(n==1)
        return 1;
    return recursive(n-1)+recursive(n-2);
}