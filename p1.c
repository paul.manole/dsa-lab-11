#include "allhead.h"

long iterative(int n)
{
    long result = 1;
    for(int i=1;i<=n;i++)
    {
        result=result*i;
    }
    return result;
}

long recursive(int n)
{
    long result=1;
    if(n==0)
    {
        return 1;
    }
    else
    return n*recursive(n-1);
}